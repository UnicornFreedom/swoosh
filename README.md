swoosh
===
Mixed genre 2D game created in 48 hours for [Mix and Game Jam (2020)](https://itch.io/jam/mix-and-game-jam-2020)

[PLAY](https://unicornfreedom.gitlab.io/swoosh)
