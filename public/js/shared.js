'use strict';

// everything in here is shared between all JS files

// constants
//--------------------------------------------------------------------------------------------------------------------//
const PI05 = Math.PI * 0.5;
const PI = Math.PI;
const PI2 = Math.PI * 2.0;

// game state
//--------------------------------------------------------------------------------------------------------------------//
let STATE = {
  // set to `true` when all resources are loaded, game objects constructed, i.e. the game is ready to be played
  loaded: false,
  // the PIXI app
  app: undefined,
  // containers
  world: undefined, ui: undefined
}

// all world objects we need links to
let WORLD = {
  // our truck
  player: undefined
}

// all UI elements we need links to
let UI = {}

const LOADER = PIXI.Loader.shared;

// helpers
//--------------------------------------------------------------------------------------------------------------------//

function sprite(texture) {
  return new PIXI.Sprite(
    LOADER.resources[texture].texture
  );
}
