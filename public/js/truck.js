'use strict';

class Truck {
  constructor(x, y) {
    this.sprite = sprite("truck");
    this.sprite.anchor.set(0.5, 0.65);
    STATE.world.addChild(this.sprite);
    this.setPosition(x, y);
    this.speed = 0.0;
    this.gearSpeed = 0.2;
    this.acceleration = 0.01;
    this.movementTimer = 0.0;
    this.gear = 0;
    this.line = 3;
    this.lineSpeed = 0;
  }

  setPosition(x, y) {
    this.x = x;
    this.y = y;
    this.sprite.position.set(x, y);
  }

  getX() { return this.x; }
  getY() { return this.y; }

  getSpeed() { return this.speed }

  setGear(value) {
    this.gear = Math.min(Math.max(-1, value), 5);
  }

  getGear() { return this.gear; }

  setLine(value) {
    this.line = Math.min(Math.max(0, value), 3);
  }

  getLine() { return this.line; }
  isChangingLine() { return this.line != this.targetLine; }

  update(delta) {
    // update line position
    let destY = WORLD.road.position.y - 192 + this.line * 128;
    let diff = destY - this.getY();
    if (Math.abs(diff) > 1) {
      if (Math.abs(diff) > 64) {
        if (Math.abs(this.lineSpeed) < 16) {
          this.lineSpeed += ((16 * Math.sign(diff)) - this.lineSpeed) * 0.1;
        }
      } else {
        this.lineSpeed = diff / 4.0;
      }
      this.setPosition(this.getX(), this.getY() + this.lineSpeed * Math.abs(this.speed));
      this.sprite.rotation = (Math.PI / 100.0) * this.lineSpeed / 16.0;
    }

    // update speed
    if (this.speed != this.gear * this.gearSpeed) {
      let diff = this.gear * this.gearSpeed - this.speed;
      if (Math.abs(diff) < this.acceleration) {
        this.speed += diff;
      } else {
        this.speed += Math.min(diff * 0.1 * delta, this.acceleration);
      }
    }
    if (this.speed != 0) {
      this.movementTimer += delta;
      this.sprite.position.set(this.getX() + Math.cos(this.movementTimer / 10.0) * this.speed * 2.0, this.getY() + Math.cos(this.movementTimer / 20.0) * this.speed);
    }
  }

  dispose() {
    STATE.app.stage.removeChild(this.sprite);
  }
}
