'use strict';

// init some graphics here
//--------------------------------------------------------------------------------------------------------------------//
PIXI.settings.SPRITE_MAX_TEXTURES = Math.min(PIXI.settings.SPRITE_MAX_TEXTURES , 16);

STATE.app = new PIXI.Application({
  width: 800,
  height: 600,
  antialias: true,
  transparent: true,
  autoDensity: true
});
STATE.app.renderer.view.style.position = "absolute";
STATE.app.renderer.view.style.display = "block";
STATE.app.renderer.resize(window.innerWidth, window.innerHeight);
document.body.appendChild(STATE.app.view);

STATE.world = new PIXI.Container();
STATE.world.sortableChildren = true;
STATE.world.x = STATE.app.view.width / 2;
STATE.world.y = STATE.app.view.height / 2;
STATE.app.stage.addChild(STATE.world);

STATE.ui = new PIXI.Container();
STATE.ui.sortableChildren = true;
STATE.app.stage.addChild(STATE.ui);

// ingnite the game cycle (loadingHandler -> setupHandler -> loopHandler)
//--------------------------------------------------------------------------------------------------------------------//
console.log("Loading resources...");

LOADER
  .add("logo", "images/logo.png")
  .add("road", "images/road.png")
  .add("grass", "images/grass.png")
  .add("truck", "images/truck.png")

LOADER.onProgress.add(loadingHandler);
LOADER.onError.add(errorHandler);
LOADER.load(setupHandler);

// called while loading resources
function loadingHandler(loader, resource) {
  console.log("> " + resource.url + " " + loader.progress + "%");
}

function errorHandler(error, loader) {
  console.log(error);
}

// called after the loader finishes its work
function setupHandler(loader, resources) {
  console.log("Done!");
  console.log("Game objects initialization...")

  WORLD.grassTop = new PIXI.TilingSprite(LOADER.resources.grass.texture, 4096, 512);
  WORLD.grassTop.anchor.set(0.5);
  WORLD.grassTop.position.set(0, -530);
  WORLD.grassTop.tileScale.y = -1.0;
  STATE.world.addChild(WORLD.grassTop);

  WORLD.grassBottom = new PIXI.TilingSprite(LOADER.resources.grass.texture, 4096, 512);
  WORLD.grassBottom.anchor.set(0.5);
  WORLD.grassBottom.position.set(0, 530);
  STATE.world.addChild(WORLD.grassBottom);

  WORLD.road = new PIXI.TilingSprite(LOADER.resources.road.texture, 4096, 564);
  WORLD.road.anchor.set(0.5);
  WORLD.road.position.set(0, 0);
  WORLD.road.tileScale.y = 1.1;
  STATE.world.addChild(WORLD.road);

  WORLD.player = new Truck(0, 192);

  let guide = new PIXI.Text('WASD / Arrows for movement, Space - brake');
  guide.x = 10;
  guide.y = 10;
  STATE.ui.addChild(guide);

  UI.speed = new PIXI.Text('SPEED: ');
  UI.speed.x = 10;
  UI.speed.y = 40;
  STATE.ui.addChild(UI.speed);

  UI.gear = new PIXI.Text('GEAR: ');
  UI.gear.x = 10;
  UI.gear.y = 70;
  STATE.ui.addChild(UI.gear);

  console.log("Done!");
  console.log("Ready... Steady... Go!")

  // run game loop
  STATE.loaded = true;
  STATE.app.ticker.add(delta => loopHandler(delta));
}

function updateTilePosition(sprite, delta) {
  sprite.tilePosition.x -= delta * WORLD.player.getSpeed() * 16;
  if (sprite.tilePosition.x < -512 || sprite.tilePosition.x > 512)
    sprite.tilePosition.x %= 512;
}

// called by Pixi's ticker at 60 FPS rate
function loopHandler(delta) {
  // update player
  WORLD.player.update(delta);

  // update road
  updateTilePosition(WORLD.grassTop, delta);
  updateTilePosition(WORLD.road, delta);
  updateTilePosition(WORLD.grassBottom, delta);

  // update UI
  UI.speed.text = "SPEED: " + WORLD.player.getSpeed();
  UI.gear.text = "GEAR: " + WORLD.player.getGear();
}

// keyboard input handler
document.addEventListener('keydown', function(event) {
  //console.log("keydown: ", event.keyCode);
  if (STATE.loaded) {
    if (event.keyCode == 87 || event.keyCode == 38) { // UP
      WORLD.player.setLine(WORLD.player.getLine() - 1);
    } else if (event.keyCode == 83 || event.keyCode == 40) { // DOWN
      WORLD.player.setLine(WORLD.player.getLine() + 1);
    } else if (event.keyCode == 65 || event.keyCode == 37) { // LEFT
      WORLD.player.setGear(WORLD.player.getGear() - 1);
    } else if (event.keyCode == 68 || event.keyCode == 39) { // RIGHT
      WORLD.player.setGear(WORLD.player.getGear() + 1);
    } else if (event.keyCode == 32) { // SPACE
      WORLD.player.setGear(0);
    }
  }
});

document.addEventListener('keyup', function(event) {
  if (STATE.loaded) {
    //
  }
});

// mouse input handler
document.addEventListener('mousemove', function(event) {
  if (STATE.loaded) {
    //
  }
});

document.addEventListener('click', function(event) {
  if (STATE.loaded) {
    //
  }
});

window.addEventListener("resize", function(event) {
  STATE.app.renderer.resize(window.innerWidth, window.innerHeight);
  STATE.world.x = STATE.app.view.width / 2;
  STATE.world.y = STATE.app.view.height / 2;
  console.log("!");
});
